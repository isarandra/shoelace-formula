import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        List<List<Integer>> listOfList= new ArrayList<>();
        System.out.print("Number of points: ");
        int points= scanner.nextInt();
        System.out.println("\nPlease type x *space* y and then hit enter for each point\n");
        for(int i=0;i<points;i++){
            List<Integer> insideList = new ArrayList<>();
            System.out.print("Point "+(i+1)+" = ");
            insideList.add(Integer.valueOf(scanner.nextInt()));
            insideList.add(Integer.valueOf(scanner.nextInt()));
            listOfList.add(insideList);
        }
        double area=0;
        for(int i=0;i<points;i++){
            if(i!=points-1){
                List<Integer> a=listOfList.get(i);
                List<Integer> b=listOfList.get(i+1);
                area+=determinants(a,b);
            }
            else{
                List<Integer> a=listOfList.get(i);
                List<Integer> b=listOfList.get(0);
                area+=determinants(a,b);
            }
        }
        System.out.println("\nArea = "+Math.abs(area/2));
    }
    static double determinants(List<Integer> list1,List<Integer> list2){
        double determinants=list1.get(0)*list2.get(1)-list1.get(1)*list2.get(0);
        return determinants;
    }
}

